package main

import (
	"encoding/hex"
	"fmt"
	"github.com/fogleman/gg"
	"image/color"
	"image/png"
	"io/ioutil"
	"os"
	"strconv"
	"strings"
	"time"
)

func getExpiryData(id string, hr bool) string {
	var date string
	// fetch expiration date from contract storage
	f, e := os.Open("./self/store/expiry_map/" + id)
	if e != nil {
		fmt.Println(e)
		os.Exit(1)
	}
	b, _ := ioutil.ReadAll(f)
	if !hr {
		return string(b)
	}
	date, _ = humanReadableDate(string(b))
	date = strings.Replace(date, "\x0a", "", 1)
	date = "Expires on: " + date
	return date
}

func printImage(ref string) {
	// ref is the string argument we are given,
	// this can be a tokenID or a domain name, such as "alice.tez"
	// if it is a numeric ID -> get the domain name:

	id := refToDomain(ref)

	expiryString := getExpiryData(id, false)

	// fetch reverse record
	var rr string
	f, e := os.Open("./self/store/records/" + id + "/address")
	if e != nil {
		fmt.Println(e)
		os.Exit(1)
	}
	b2, _ := ioutil.ReadAll(f)
	if len(b2) >= 10 {
		rr = string(b2)
		rr = strings.Replace(rr, "\x0a", "", 1)
	}

	// Load template.png
	img, err := gg.LoadImage("./self/dmeta/assets/template.png")
	if err != nil {
		panic(err)
	}

	dc := gg.NewContextForImage(img)
	font := "./self/dmeta/assets/Roboto-Medium.ttf"
	// Rectangle in which the text needs to be drawn
	drawText(dc, 100, 400, 50, id, font)
	if len(rr) >= 5 {
		drawText(dc, 100, 550, 20, "Points to: "+rr, font)
	}
	drawText(dc, 100, 600, 20, expiryString, font)
	printHeadersPNG()
	hr := hex.NewEncoder(os.Stdout)
	png.Encode(hr, dc.Image())
}

func drawText(dc *gg.Context, x, y, fontsize float64, text, fontPath string) {
	if err := dc.LoadFontFace(fontPath, fontsize); err != nil {
		fmt.Println(err)
		panic(err)
	}
	white, _ := parseHexColor("#ffffff")
	dc.SetColor(white)
	tx := x
	ty := y // Adjusting by fontSize since the y-coordinate in gg is the baseline of the text
	dc.DrawString(text, tx, ty)
}

// parseHexColor parses a color hex string like "#335eea" into a color.RGBA value.
func parseHexColor(s string) (color.RGBA, error) {
	if len(s) != 7 || s[0] != '#' {
		return color.RGBA{}, fmt.Errorf("invalid color format")
	}

	r, err := strconv.ParseUint(s[1:3], 16, 8)
	if err != nil {
		return color.RGBA{}, err
	}

	g, err := strconv.ParseUint(s[3:5], 16, 8)
	if err != nil {
		return color.RGBA{}, err
	}

	b, err := strconv.ParseUint(s[5:7], 16, 8)
	if err != nil {
		return color.RGBA{}, err
	}

	return color.RGBA{R: uint8(r), G: uint8(g), B: uint8(b), A: 255}, nil
}

func humanReadableDate(s string) (string, error) {
	s = strings.Trim(s, "\x0a")
	t, err := time.Parse(time.RFC3339, s)
	if err != nil {
		fmt.Println(err)
		return "", err
	}

	day := t.Day()
	suffix := "th"
	switch day {
	case 1, 21, 31:
		suffix = "st"
	case 2, 22:
		suffix = "nd"
	case 3, 23:
		suffix = "rd"
	}

	d := fmt.Sprintf("%d%s %s, %d", day, suffix, t.Month(), t.Year())
	return d, nil
}
