package main

import (
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
	"strings"
)

func main() {
	CMD := os.Getenv("CMD")
	cmds := strings.Split(CMD, "/")
	if CMD == "contract/metadata" {
		getContractMetadata()
		return
	}
	if CMD == "capabilities" {
		printCapabilities()
		return
	}
	if len(cmds) == 3 {
		if cmds[0] == "token" && cmds[2] == "image" {
			printImage(cmds[1])
			return
		}
		if cmds[0] == "token" && cmds[2] == "metadata" {
			getTokenMetadata(cmds[1])
			return
		}
	} else {
		printHeaders()
		fmt.Println("Command not implemented")
		return
	}
}

func printCapabilities() {
	printHeadersJSON()
	caps := []string{
		"contract/metadata",
		"token/:id/metadata",
		"token/:id/image",
	}
	b,_ := json.Marshal(caps)
	fmt.Println(string(b))
}

func refToDomain(ref string) string {
	var id string
	if _, e := strconv.Atoi(ref); e == nil {
		// ref is a number - tokenId
		// fetch domain name from contract storage
		f, e := os.Open("./self/store/tzip12_tokens/" + ref)
		if e != nil {
			fmt.Println(e)
			os.Exit(1)
		}
		b, _ := ioutil.ReadAll(f)
		buf := make([]byte, 1024)
		n, _ := hex.Decode(buf, b)
		id = string(buf[:n])
	} else {
		// ref is a domain already
		id = ref
	}
	return id
}


func printHeadersPNG() {
	fmt.Println("Content-Type: image/png")
	fmt.Println("Encoding: hex")
	fmt.Println("Compression: none")
	fmt.Println("Body:")
}

func printHeaders() {
	fmt.Println("Content-Type: application/text")
	fmt.Println("Encoding: none")
	fmt.Println("Compression: none")
	fmt.Println("Body:")
}

func printHeadersJSON() {
	fmt.Println("Content-Type: application/json")
	fmt.Println("Encoding: none")
	fmt.Println("Compression: none")
	fmt.Println("Body:")
}


