#!/usr/bin/env bash

compile_wasm() {
    tinygo build -target=wasi -o dmeta.wasm main
    cp dmeta.wasm ./build/dmeta.wasm
    echo "dmeta wasm module compiled and stored in ./build/dmeta.wasm"
}


compile_wasm
