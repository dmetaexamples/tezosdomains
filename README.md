# WIP Tezos Domains example module

This is an example dmeta module for tezos domains,
specifically for the contract `KT1GBZmSxmnKJXGMdMLbugPfLyUPmuLSMwKS` on mainnet.

It implements:

* contractMetadata
* tokenMetadata
* NFT image rendering

Generally, when the dmeta proxy looks for a wasm module to execute,
it does so by looking into the `dmeta` BigMap in contract storage,
since the Tezos Domains Name Registry was deployed before the dmeta standard
was developed, we can make an exception and provide the data for this contract
via the dmeta config file like so:

``` json

{
  // rest of config
  "MockStorage": {
    "KT1GBZmSxmnKJXGMdMLbugPfLyUPmuLSMwKS" : {
      "dmeta": {
        "module":"ipfs://QmbxRJjq5NfZrTCJyetS61Q827pqKAhsrD3qEXzwLQFiHm",
        "assets":"ipfs://QmUvNQwNP5kDPMhPR5VmFLKXwV74iBWWHhSo8EfBeqHBUz"
      }
    }
  }
}
```

In newer contracts, the contract storage must include such data,
**We will not manually add metadata to the default config for contracts originated after dmeta was released.**



