package main

import (
	"fmt"
	"os"
	"io"
	"encoding/hex"
	"encoding/json"
	"io/ioutil"
	"strconv"
	"bytes"
)


type TokenMetadata struct {
	Name       string `json:"name"`
	Symbol     string `json:"symbol"`
	Decimals   string `json:"decimals"`
	DomainData map[string]string `json:"domainData"`
	IsBooleanAmount bool `json:"isBooleanAmount"`
	DisplayURI string `json:"displayUri"`
	ArtifactURI string `json:"artifactUri"`
	Attributes []Attribute `json:"attributes"`
}

type Attribute struct {
	Name string `json:"name"`
	Value string `json:"value"`
}

type Record struct {
	Address string `json:"address"`
	Data    map[string]string `json:"data"`
	ExpiryKey    interface{} `json:"expiry_key"`
	InternalData struct {
	} `json:"internal_data"`
}


func getContractMetadata(){
	printHeadersJSON()
	f,_ := os.Open("./self/dmeta/assets/contractMetadata.json")
	io.Copy(os.Stdout, f)
}


func getTokenMetadata(ref string) {
	domain := refToDomain(ref)
	f,e := os.Open("./self/store/records/" + domain + "/_data")
	if e != nil {
		fmt.Println(e)
		os.Exit(1)
	}
	b,_ := ioutil.ReadAll(f)
	record := Record{}
	json.Unmarshal(b,&record)
	md := TokenMetadata{}
	md.Name = domain
	md.Symbol = "TD"
	md.IsBooleanAmount = true
	md.Decimals = "0"
	md.DomainData = map[string]string{}
	for k,v := range record.Data {
		vs,_ := hex.DecodeString(v)
		vs = bytes.Trim(vs, "\"")
		md.DomainData[k] = string(vs)
	}
	md.DisplayURI = "dmeta://self/token/" + domain + "/image"
	md.ArtifactURI = md.DisplayURI
	md.Attributes = []Attribute{
		{
			Name: "Length",
			Value: strconv.Itoa(len(domain)-4),
		},
		{
			Name: "Expiry-Date",
			Value: getExpiryData(domain, false),

		},
	}
	res,_ := json.Marshal(md)
	printHeadersJSON()
	fmt.Println(string(res))
}
